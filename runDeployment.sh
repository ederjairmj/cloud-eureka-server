o "Deploying app.jar to docker folder"
packageName=`ls target/commmon-eureka-server*.jar`
versionid=`echo $packageName | awk -F "-" '{ print $2}'`
versionname=`echo $packageName | awk -F "-" '{ print $3}' | awk -F "." '{ print $1}'`
version=`echo $versionid-$versionname`
echo "version: $version"
cp -r $packageName deployment/app.jar
dockerImageName=ederjairmj/eureka-server
dockerRedName=employee
dockerContainerName=config-eureka-ejmj
dockerpid=`docker ps -a | grep $dockerImageName | grep "Up" | awk -F " " '{ print $1 }'`
if [[ $dockerpid != "" ]];then 
   docker kill $dockerpid
   docker rm $dockerpid
fi
docker build -t $dockerImageName deployment/.
#docker run -d --name=$dockerRedName --mount type=bind,source=C:\static,target=/opt/static --network=$dockerRedName -p 6070:6070 $dockerImageName 
docker run -d --name=$dockerContainerName --network=$dockerRedName -p 8010:8010 $dockerImageName 
dockerImageId=`docker images | grep $dockerImageName | grep latest | awk -F " " '{print $3}'` 
docker tag $dockerImageId $dockerImageName:$version
docker login -u ederjairmj -p ederjairmj.3
docker push $dockerImageName:$version
